# Complete the find_second_largest function which accepts
# a list of numerical values and returns the second largest
# in the list
#
# If the list of values is empty, the function should
# return None
#
# If the list of values has only one value, the function
# should return None
#
# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def find_second_largest(values):
    if values == []:
        return None
    ordered_nums = sorted(values)
    second_largest_index = len(ordered_nums) - 2
    if second_largest_index < 0:
        return None
    return ordered_nums[second_largest_index]

print(find_second_largest([1,3,7,12,9,6,8]))
print(find_second_largest([1]))

# DONE