# Complete the check_password function that accepts a
# single parameter, the password to check.
#
# A password is valid if it meets all of these criteria
#   * It must have at least one lowercase letter (a-z)
#   * It must have at least one uppercase letter (A-Z)
#   * It must have at least one digit (0-9)
#   * It must have at least one special character $, !, or @
#   * It must have six or more characters in it
#   * It must have twelve or fewer characters in it
#
# The string object has some methods that you may want to use,
# like ".isalpha", ".isdigit", ".isupper", and ".islower"

def check_password(password):
    is_lower = False
    is_upper = False
    is_digit = False
    is_special = False
    is_length = False

    for i in password:
        if i.islower():
            is_lower = True
        if i.isupper():
            is_upper = True
        if i.isdigit():
            is_digit = True
        if i == "$" or i == "!" or i == "@":
            is_special = True
        if 6 <= len(password) <= 12:
            is_length = True

    if is_lower and is_upper and is_digit and is_special and is_length:
        return "This password is acceptable"
    else:
        return "Bad password"


print(check_password("Swag27!"))

# DONE
