# Complete the has_quorum function to test to see if the number
# of the people in the attendees list is greater than or equal
# to 50% of the number of people in the members list.

def has_quorum(attendees_list, members_list):
    # if length of attendees is greater than half the length of members
    if len(attendees_list) >= len(members_list) / 2:
        return True
    else:
        return False
        # return true

print(has_quorum([1,2,3,4],[1,2,3,4,5,6,7,8,9,10])) # False
print(has_quorum([1,2,3,4,5],[1,2,3,4,5,6,7,8,9,10])) # True

# DONE
