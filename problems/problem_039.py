# Complete the reverse_dictionary function which has a
# single parameter that is a dictionary. Return a new
# dictionary that has the original dictionary's values
# for its keys, and the original dictionary's keys for
# its values.
#
# Examples:
#   * input:  {}
#     output: {}
#   * input:  {"key": "value"}
#     output: {"value", "key"}
#   * input:  {"one": 1, "two": 2, "three": 3}
#     output: {1: "one", 2: "two", 3: "three"}

# def reverse_dictionary(dictionary):
#     # for dict will be y: x for given x,y in the .items of the dictionary
#     dictionary = {y: x for x, y in dictionary.items()}
#     return dictionary
#     pass


# print(reverse_dictionary({"key": "value"}))

# DONE

student = {"name": "Andrew", "age": 27, "language": "Python"}

# # Get Value
# print(student["age"])
# print(student["name"])
# print(student["language"])
# # print(student["grad_date"])

# # OR

# print(student.get("age"))
# print(student.get("name"))
# print(student.get("language"))
# print(student.get("grad_date"))

# # Set a Value / Update Value
# student["grad_date"] = "05/03/2023"
# print(student.get("grad_date"))
# student["age"] = 28
# print(student.get("age"))

# WONT WORK
# for key, value in student:
#     print(key)
#     print(value)

# WILL WORK
# my_dict = {}
# for key,value in student.items():
#     student[value] = key
#     print(key)
#     print(value)
# print(student)


def reverse_dictionary(dictionary):
    reverse_dictionary = {}
    for key,value in dictionary.items():
        reverse_dictionary[value] = key
    return reverse_dictionary

revered_student = reverse_dictionary(student)
print(student)
print(revered_student)

for key in student:
    print(key)              # Returns keys
    print(student[key])     # Returns values

for value in student.values():
    print(value)            # Prints values