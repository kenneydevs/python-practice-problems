# Write a function that meets these requirements.
#
# Name:       shift_letters
# Parameters: a string containing a single word
# Returns:    a new string with all letters replaced
#             by the next letter in the alphabet
#
# If the letter "Z" or "z" appear in the string, then
# they would get replaced by "A" or "a", respectively.
#
# Examples:
#     * inputs:  "import"
#       result:  "jnqpsu"
#     * inputs:  "ABBA"
#       result:  "BCCB"
#     * inputs:  "Kala"
#       result:  "Lbmb"
#     * inputs:  "zap"
#       result:  "abq"
#
# You may want to look at the built-in Python functions
# "ord" and "chr" for this problem

def shift_leaders(string):
    list = []

    for i in string:
        i = ord(i)

        if i == 122:
            i = 96
        elif i == 90:
            i = 64

        i = chr(i + 1)

        list.append(i)

    list = "".join(list)

    return list

print(shift_leaders("import"))      # "jnqpsu"
print(shift_leaders("ABBA"))        # "BCCB"
print(shift_leaders("kala"))        # "lbmb"
print(shift_leaders("Zap"))         # "Abq"

# DONE