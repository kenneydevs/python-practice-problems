# Write a function that meets these requirements.
#
# Name:       halve_the_list
# Parameters: a single list
# Returns:    two lists, each containing half of the original list
#             if the original list has an odd number of items, then
#             the extra item is in the first list
#
# Examples:
#    * input: [1, 2, 3, 4]
#      result: [1, 2], [3, 4]
#    * input: [1, 2, 3]
#      result: [1, 2], [3]

def halve_the_list(list):
    # find length
    half_length = len(list) / 2
    # iterate over list until half length
    # for i in list
        # return list1
    # iterate over list from half to length
        # return list2
    # return list1, list2
    pass

print(halve_the_list([1, 2, 3, 4]))
print(halve_the_list([1, 2, 3]))

# NEEDS REVIEW!!!