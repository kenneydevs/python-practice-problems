# Complete the remove_duplicate_letters that takes a string
# parameter "s" and returns a string with all of the
# duplicates removed.
#
# Examples:
#   * For "abc", the result is "abc"
#   * For "abcabc", the result is "abc"
#   * For "abccba", the result is "abc"
#   * For "abccbad", the result is "abcd"
#
# If the list is empty, then return the empty string.

def remove_duplicate_letters(s):
    if s == "":
        return None
    no_dupes = ""
    for letter in s:
        if letter not in no_dupes:
            no_dupes += letter
    return no_dupes

print(remove_duplicate_letters("abcabc"))
print(remove_duplicate_letters("abccba"))
print(remove_duplicate_letters("abccbad"))
print(remove_duplicate_letters(""))

# DONE
